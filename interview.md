```js
const testArr = [
  { name: 'xiaohong', like: 'singing' },
  { name: 'daming', like: 'football' },
  { name: 'zhangzhang', like: 'basketball' },
  { name: 'xiaohong', like: 'dancing' },
]

{
  xiaohong: {
    like: [ 'singing', 'dancing' ]
  },
  daming: {
    like: [ 'football' ]
  },
  zhangzhang: {
    like: [ 'basketball' ]
  }
}
```